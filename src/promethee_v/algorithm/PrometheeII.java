package promethee_v.algorithm;

import promethee_v.model.Alternative;
import promethee_v.model.Criteria;
import promethee_v.model.PreferenceTactics;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Promethee II multi-criteria decision making method.
 *
 * Created by biela.arek@gmail.com (Arek Biela) on 20.12.2016.
 */
public class PrometheeII {

    /**List of all possible alternatives */
    private final ArrayList<Alternative> alternativesList;
    /**List of all criteria included in the problem */
    private final ArrayList<Criteria> criteriaList;
    /**Performance table*/
    private final ArrayList<List<Integer>> performanceTable;
    /**Criteria weight values mapped by criteria*/
    private final HashMap<Criteria, Double> criteriaWeightsMap;
    /**Criteria's preference threshold value mapped by criteria */
    private final HashMap<Criteria, Double> preferenceThresholdValuesMap;
    /**Criteria's indifference threshold value mapped by criteria */
    private final HashMap<Criteria, Double> indifferenceThresholdValuesMap;
    /**Preference computation tactics */
    private final PreferenceTactics preferenceTactics;

    private HashMap<Alternative, Double> alternativeNetFlowValuesMap;

    public PrometheeII(ArrayList<Alternative> alternativesList, ArrayList<Criteria> criteriaList,
                       ArrayList<List<Integer>> performanceTable, HashMap<Criteria, Double> criteriaWeightsMap,
                       HashMap<Criteria, Double> preferenceThresholdValuesMap, HashMap<Criteria, Double> indifferenceThresholdValuesMap,
                       PreferenceTactics preferenceTactics) {
        this.alternativesList = alternativesList;
        this.criteriaList = criteriaList;
        this.performanceTable = performanceTable;
        this.criteriaWeightsMap = criteriaWeightsMap;
        this.preferenceThresholdValuesMap = preferenceThresholdValuesMap;
        this.indifferenceThresholdValuesMap = indifferenceThresholdValuesMap;
        this.preferenceTactics = preferenceTactics;
        this.alternativeNetFlowValuesMap = new HashMap<>();
    }

    /**
     * Returns double array with filled net flow values for each alternative in primary order.
     *
     * @return - net flow values array
     */
    public double[] getNetFlowValuesInAlternativeOrder() {
        if (alternativeNetFlowValuesMap.isEmpty()) {
            calculateAlternativeNetFlow();
        }

        double[] alternativeNetFlowArray = new double[alternativesList.size()];
        for (int i = 0; i < alternativesList.size(); i++) {
            alternativeNetFlowArray[i] = alternativeNetFlowValuesMap.get(alternativesList.get(i));
        }

        return alternativeNetFlowArray;
    }

    /**
     * Returns linked hash map with alternative ranking ordered by net flow.
     *
     * @return - linked hash map ranking
     */
    public LinkedHashMap<Alternative, Double> getAlternativeRankingByNetFlow() {
        if (alternativeNetFlowValuesMap.isEmpty()) {
            System.out.println("PrometheeII - started calculating alternative net flow");
            calculateAlternativeNetFlow();
            System.out.println("PrometheeII - alternative net flow calculating has been finished!");
        }

        return alternativeNetFlowValuesMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    /**Get the preference value for selected criteria, based on two alternatives put in order. */
    private double getPreferenceValueFor(Criteria criteria, Alternative firstAlternative, Alternative secondAlternative) {
        int selectedCriteriaIndex = criteriaList.indexOf(criteria);
        double criteriaPreferenceThreshold = preferenceThresholdValuesMap.get(criteria);
        double criteriaIndifferenceThreshold = indifferenceThresholdValuesMap.get(criteria);
        double criteriaValueFirstAlternative = performanceTable.get(alternativesList.indexOf(firstAlternative)).get(selectedCriteriaIndex);
        double criteriaValueSecondAlternative = performanceTable.get(alternativesList.indexOf(secondAlternative)).get(selectedCriteriaIndex);
        return preferenceTactics.getPreferenceValue(criteriaValueFirstAlternative, criteriaValueSecondAlternative, criteriaPreferenceThreshold, criteriaIndifferenceThreshold);
    }

    private double getPreferenceValueBetween(Alternative firstAlternative, Alternative secondAlternative) {
        double preferenceValue = 0;
        for (Criteria criteria : criteriaList) {
            preferenceValue += criteriaWeightsMap.get(criteria) * getPreferenceValueFor(criteria, firstAlternative, secondAlternative);
        }
        return preferenceValue;
    }

    private double getPositiveFlowValueFor(Alternative alternative) {
        double positiveFlowValue = 0;
        for (Alternative nextAlternative : alternativesList) {
            positiveFlowValue += getPreferenceValueBetween(alternative, nextAlternative);
        }
        return positiveFlowValue;
    }

    private double getNegativeFlowValueFor(Alternative alternative) {
        double negativeFlowValue = 0;
        for (Alternative nextAlternative : alternativesList) {
            negativeFlowValue += getPreferenceValueBetween(nextAlternative, alternative);
        }
        return negativeFlowValue;
    }

    private double getNetFlowValueFor(Alternative alternative) {
        return getPositiveFlowValueFor(alternative) - getNegativeFlowValueFor(alternative);
    }

    private void calculateAlternativeNetFlow() {
        for (Alternative alternative : alternativesList) {
            System.out.println("Calculating net flow for alternative: " + alternative.getName());
            alternativeNetFlowValuesMap.put(alternative, getNetFlowValueFor(alternative));
        }
    }
}
