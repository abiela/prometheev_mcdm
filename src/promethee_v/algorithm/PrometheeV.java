package promethee_v.algorithm;

import org.apache.commons.math.optimization.GoalType;
import org.apache.commons.math.optimization.OptimizationException;
import org.apache.commons.math.optimization.RealPointValuePair;
import org.apache.commons.math.optimization.linear.LinearConstraint;
import org.apache.commons.math.optimization.linear.LinearObjectiveFunction;
import org.apache.commons.math.optimization.linear.Relationship;
import org.apache.commons.math.optimization.linear.SimplexSolver;
import promethee_v.model.Alternative;
import promethee_v.model.Criteria;
import promethee_v.model.PreferenceTactics;
import promethee_v.model.PrometheeConstraint;

import java.util.*;

/**
 * Calculates optimal value solution with its subset, based on Promethee V multi-criteria decision making method.
 *
 * Created by biela.arek@gmail.com (Arek Biela) on 20.12.2016.
 */
public class PrometheeV {

    /** List of all possible alternatives*/
    private ArrayList<Alternative> alternativesList;
    /** List of defined criteriaWeights*/
    private ArrayList<Criteria> criteriaList;
    /** Performance table*/
    private ArrayList<List<Integer>> performanceTable;
    /** List of Promethee V constraints*/
    private ArrayList<PrometheeConstraint> constraints;
    /** Promethee II method object*/
    private PrometheeII prometheeIIMethod;
    /** Set of selected alternatives with appropriate net flow values*/
    private LinkedHashMap<Alternative, Double> solutionSubsetMap;
    /** Optimal problem value*/
    private double optimalValue;

    public PrometheeV(ArrayList<Alternative> alternativesList, ArrayList<Criteria> criteriaList,
                      ArrayList<List<Integer>> performanceTable, HashMap<Criteria, Double> criteriaWeightsMap,
                      HashMap<Criteria, Double> preferenceThresholdValuesMap, HashMap<Criteria, Double> indifferenceThresholdValuesMap,
                      PreferenceTactics preferenceTactics, ArrayList<PrometheeConstraint> constraints) {

        this.alternativesList = alternativesList;
        this.criteriaList = criteriaList;
        this.performanceTable = performanceTable;
        this.constraints = constraints;
        this.prometheeIIMethod = new PrometheeII(alternativesList, criteriaList,
                performanceTable, criteriaWeightsMap, preferenceThresholdValuesMap,
                indifferenceThresholdValuesMap, preferenceTactics);
        this.solutionSubsetMap = new LinkedHashMap<>();
    }

    /**
     * Returns an optimal solved problem value.
     */
    public double getOptimalValue() {
        if (solutionSubsetMap.isEmpty())
            findAlternativeSet();

        if (optimalValue == 0) {
            for (Map.Entry<Alternative, Double> alternativeDoubleEntry : solutionSubsetMap.entrySet()) {
                optimalValue += alternativeDoubleEntry.getValue();
            }
        }

        return optimalValue;
    }

    /**
     * Returns an optimal subset of alternatives according to solved problem.
     */
    public Set<Alternative> getOptimalAlternativeSet() {
        if (solutionSubsetMap.isEmpty())
            findAlternativeSet();

        Set<Alternative> optimalAlternativeSet = new HashSet<>();
        for (Map.Entry<Alternative, Double> alternativeDoubleEntry : solutionSubsetMap.entrySet()) {
            optimalAlternativeSet.add(alternativeDoubleEntry.getKey());
        }

        return optimalAlternativeSet;
    }

    /**
     * Triggers linear programming optimisation based on Simplex solver.
     */
    private void findAlternativeSet() {
        //Get obtained net flow values from Promethee II
        double[] netFlowValues = getPrometheeIINetFlowValues();
        //Construct Linear Programming Problem
        LinearObjectiveFunction f = new LinearObjectiveFunction(netFlowValues, 0);

        //Add problem constraints
        Collection<LinearConstraint> problemConstraints = new ArrayList<>();
        problemConstraints.addAll(getProvidedConstraints(netFlowValues.length));
        problemConstraints.addAll(getBinaryConstraints(netFlowValues.length));

        //Create and run optimization solver
        RealPointValuePair solution = null;
        try {
            SimplexSolver simplexSolver = new SimplexSolver(0.1);
            simplexSolver.setMaxIterations(10000);
            solution = simplexSolver.optimize(f, problemConstraints, GoalType.MAXIMIZE, true);
        } catch (OptimizationException e) {
            e.printStackTrace();
        }

        //Save the solution
        if (solution != null) {
            int solutionSize = solution.getPoint().length;
            for (int i = 0; i < solutionSize; i++) {
                double nextPoint = solution.getPoint()[i];
                if (nextPoint >= 1.0d)
                    solutionSubsetMap.put(alternativesList.get(i), netFlowValues[i]);
            }
        }
    }

    /**
     * Providing binary constraints for alternative selection variable
     *
     * @param alternativeAmount - number of alternatives in the set
     * @return
     */
    private Collection<LinearConstraint> getBinaryConstraints(int alternativeAmount) {
        Collection<LinearConstraint> binaryConstraintCollection = new ArrayList<>();
        for (int i = 0; i < alternativeAmount; i++) {
            double[] alternativesIndicators = new double[alternativeAmount];
            alternativesIndicators[i] = 1;
            LinearConstraint greaterBinaryConstraint = new LinearConstraint(alternativesIndicators, Relationship.GEQ, 0);
            LinearConstraint lessBinaryConstraint = new LinearConstraint(alternativesIndicators, Relationship.LEQ, 1);
            binaryConstraintCollection.add(greaterBinaryConstraint);
            binaryConstraintCollection.add(lessBinaryConstraint);
        }
        return binaryConstraintCollection;
    }

    /**
     * Providing custom constraints that are put by decision maker.
     * @param alternativeAmount - number of alternatives in the set
     * @return
     */
    private Collection<LinearConstraint> getProvidedConstraints(int alternativeAmount) {
        Collection<LinearConstraint> constraintCollection = new ArrayList<>();
        //Iterate through all of provided custom constraints
        for (PrometheeConstraint prometheeConstraint : constraints) {
            double[] alternativesIndicators = new double[alternativeAmount];
            int criteriaIndex = criteriaList.indexOf(prometheeConstraint.getCriteria());
            for (List<Integer> integers : performanceTable) {
                alternativesIndicators[performanceTable.indexOf(integers)] = integers.get(criteriaIndex);
            }
            LinearConstraint customConstaint = new LinearConstraint(alternativesIndicators,
                    getRelationship(prometheeConstraint.getRelationship()), prometheeConstraint.getConstraintValue());
            constraintCollection.add(customConstaint);
        }
        return constraintCollection;
    }

    /**
     * Retrieve library's relationship object based on provided custom criteria.
     * @param relationship - Promethee constraint relationship value
     * @return
     */
    private Relationship getRelationship(promethee_v.model.Relationship relationship) {
        if (relationship == promethee_v.model.Relationship.EQUAL)
            return Relationship.EQ;

        if (relationship == promethee_v.model.Relationship.GREATER_OR_EQUAL)
            return Relationship.GEQ;

        if (relationship == promethee_v.model.Relationship.LESS_OR_EQUAL)
            return Relationship.LEQ;

        return Relationship.EQ;
    }

    /**
     * Returns net flow values based on Promethee II computation.
     */
    private double[] getPrometheeIINetFlowValues() {
        return prometheeIIMethod.getNetFlowValuesInAlternativeOrder();
    }
}
