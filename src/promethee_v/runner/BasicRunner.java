package promethee_v.runner;

import promethee_v.algorithm.PrometheeII;
import promethee_v.algorithm.PrometheeV;
import promethee_v.loader.DataLoaderCsv;
import promethee_v.model.Alternative;
import promethee_v.model.Criteria;
import promethee_v.model.PreferenceTactics;
import promethee_v.model.PrometheeConstraint;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by biela.arek@gmail.com (Arek Biela) on 20.12.2016.
 */
public class BasicRunner {

    public static void main(String[] args) {

        String[] argsTable = args;
        if (argsTable.length != 0) {
            String selectedDataset = argsTable[0];
            String criteriaDir = null;
            if (argsTable.length > 1) {
                criteriaDir = argsTable[1];
            }

            if (selectedDataset.equals(RunnerCase.CREDITS.getCmdLineShortcut())) {
                runPrometheeVWith(RunnerCase.CREDITS, criteriaDir != null ? criteriaDir : "");
            }

            if (selectedDataset.equals(RunnerCase.CARS.getCmdLineShortcut())) {
                runPrometheeVWith(RunnerCase.CARS, criteriaDir != null ? criteriaDir : "");
            }
        }
    }

    public static void runPrometheeIIWith(RunnerCase runnerCase) {
        //Load data with loader usage
        ArrayList<Alternative> alternativeList = DataLoaderCsv.loadAlternativesFrom(runnerCase.getAlternativeResourceString(), runnerCase.getSeparatorRegex());
        ArrayList<Criteria> criteriaList = DataLoaderCsv.loadCriteriaFrom(runnerCase.getCriteriaResourceString(), runnerCase.getSeparatorRegex());
        ArrayList<List<Integer>> performanceTableList = DataLoaderCsv.loadPerformanceTableFrom(runnerCase.getPerformanceTableResourceString(), runnerCase.getSeparatorRegex());
        ArrayList<PrometheeConstraint> constraintList = DataLoaderCsv.loadCriteriaConstraintsFrom(runnerCase.getConstraintsResourceString(), runnerCase.getSeparatorRegex());

        //Prepare maps
        HashMap<Criteria, Double> criteriaWeightsMap = new HashMap<>();
        HashMap<Criteria, Double> preferenceThresholdMap = new HashMap<>();
        HashMap<Criteria, Double> indifferenceThresholdMap = new HashMap<>();

        for (Criteria criteria : criteriaList) {
            indifferenceThresholdMap.put(criteria, runnerCase.getBasicIndifferenceThreshold());
            preferenceThresholdMap.put(criteria, runnerCase.getBasicPreferenceThreshold());
            criteriaWeightsMap.put(criteria, runnerCase.getBasicCriteriaWeightValue());
        }

        //Prepare Promethee II object
        PrometheeII prometheeII = new PrometheeII(alternativeList, criteriaList, performanceTableList, criteriaWeightsMap,
                preferenceThresholdMap, indifferenceThresholdMap, PreferenceTactics.V_SHAPE_WITH_INDIFFERENCE);

        System.out.println("Alternatives ranking base on Promethee II method: \n\n");
        LinkedHashMap<Alternative, Double> alternativeRanking = prometheeII.getAlternativeRankingByNetFlow();
        for (Map.Entry<Alternative, Double> alternativeDoubleEntry : alternativeRanking.entrySet()) {
            Alternative alternative = alternativeDoubleEntry.getKey();
            Double netFlowValue = alternativeDoubleEntry.getValue();
            System.out.println("Alternative: " + alternative.getName() + " - " + netFlowValue);
        }
    }

    public static void runPrometheeVWith(RunnerCase runnerCase, String criteriaDir) {
        //Load data with loader usage
        System.out.println("Launched method Promethee V");
        long startTimestamp = System.currentTimeMillis();
        ArrayList<Alternative> alternativeList = DataLoaderCsv.loadAlternativesFrom(runnerCase.getAlternativeResourceString(), runnerCase.getSeparatorRegex());
        ArrayList<Criteria> criteriaList = DataLoaderCsv.loadCriteriaFrom(runnerCase.getCriteriaResourceString(), runnerCase.getSeparatorRegex());
        ArrayList<List<Integer>> performanceTableList = DataLoaderCsv.loadPerformanceTableFrom(runnerCase.getPerformanceTableResourceString(), runnerCase.getSeparatorRegex());
        ArrayList<PrometheeConstraint> constraintList = DataLoaderCsv.loadCriteriaConstraintsFrom(!criteriaDir.isEmpty() ? criteriaDir : runnerCase.getConstraintsResourceString(), runnerCase.getSeparatorRegex());

        //Prepare maps
        HashMap<Criteria, Double> criteriaWeightsMap = new HashMap<>();
        HashMap<Criteria, Double> preferenceThresholdMap = new HashMap<>();
        HashMap<Criteria, Double> indifferenceThresholdMap = new HashMap<>();

        for (Criteria criteria : criteriaList) {
            indifferenceThresholdMap.put(criteria, runnerCase.getBasicIndifferenceThreshold());
            preferenceThresholdMap.put(criteria, runnerCase.getBasicPreferenceThreshold());
            criteriaWeightsMap.put(criteria, runnerCase.getBasicCriteriaWeightValue());
        }

        //Prepare Promethee V object
        PrometheeV prometheeV = new PrometheeV(alternativeList, criteriaList, performanceTableList,
                criteriaWeightsMap, preferenceThresholdMap, indifferenceThresholdMap,
                PreferenceTactics.V_SHAPE_WITH_INDIFFERENCE, constraintList);

        Set<Alternative> optimalAlternatives = prometheeV.getOptimalAlternativeSet();
        for (Alternative optimalAlternative : optimalAlternatives) {
            System.out.println("Optimal alternative: " + optimalAlternative.getName());
        }
        long timeExpired = System.currentTimeMillis() - startTimestamp;
        System.out.println("Optimal value solution: " + prometheeV.getOptimalValue());
        System.out.println("Alternatives amount in optimal subset: " + optimalAlternatives.size());
        System.out.println("Promethee V algorithm execution time: " + String.valueOf(TimeUnit.MILLISECONDS.toSeconds(timeExpired)) + " seconds.");
    }
}
