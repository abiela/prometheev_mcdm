package promethee_v.runner;

/**
 * Runner case constants for launch facilitating.
 *
 * Created by biela.arek@gmail.com (Arek Biela) on 01.01.2017.
 */
public enum RunnerCase {

    CARS() {
        @Override
        public String getAlternativeResourceString() {
            return "carsAlternatives.csv";
        }

        @Override
        public String getCriteriaResourceString() {
            return "carsCriteria.csv";
        }

        @Override
        public String getPerformanceTableResourceString() {
            return "carsPerformanceTable.csv";
        }

        @Override
        public String getConstraintsResourceString() {
            return "carsConstraints.csv";
        }

        @Override
        public String getSeparatorRegex() {
            return ",";
        }

        @Override
        public double getBasicIndifferenceThreshold() {
            return 0;
        }

        @Override
        public double getBasicCriteriaWeightValue() {
            return 1;
        }

        @Override
        public double getBasicPreferenceThreshold() {
            return 2;
        }

        @Override
        public String getCmdLineShortcut() {
            return "cars";
        }
    },

    CREDITS() {
        @Override
        public String getAlternativeResourceString() {
            return "germanCreditAlternatives.csv";
        }

        @Override
        public String getCriteriaResourceString() {
            return "germanCreditCriteria.csv";
        }

        @Override
        public String getPerformanceTableResourceString() {
            return "germanCreditPerformanceTable.csv";
        }

        @Override
        public String getConstraintsResourceString() {
            return "germanCreditConstraints.csv";
        }

        @Override
        public String getSeparatorRegex() {
            return ";";
        }

        @Override
        public double getBasicIndifferenceThreshold() {
            return 0;
        }

        @Override
        public double getBasicCriteriaWeightValue() {
            return 1;
        }

        @Override
        public double getBasicPreferenceThreshold() {
            return 2;
        }

        @Override
        public String getCmdLineShortcut() {
            return "credits";
        }
    };

    public abstract String getAlternativeResourceString();

    public abstract String getCriteriaResourceString();

    public abstract String getPerformanceTableResourceString();

    public abstract String getConstraintsResourceString();

    public abstract String getSeparatorRegex();

    public abstract double getBasicIndifferenceThreshold();

    public abstract double getBasicCriteriaWeightValue();

    public abstract double getBasicPreferenceThreshold();

    public abstract String getCmdLineShortcut();
}
