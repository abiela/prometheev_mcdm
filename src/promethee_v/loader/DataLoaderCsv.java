package promethee_v.loader;

import promethee_v.model.Alternative;
import promethee_v.model.Criteria;
import promethee_v.model.DataConstants;
import promethee_v.model.PrometheeConstraint;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by biela.arek@gmail.com (Arek Biela) on 20.12.2016.
 */
public class DataLoaderCsv {

    /**
     * Loads alternatives from specified file
     * @param fileName - name of alternatives file
     * @param separatorRegex - regular expression that describes the splitter
     * @return - list of possible alternatives
     */
    public static ArrayList<Alternative> loadAlternativesFrom(String fileName, String separatorRegex) {
        if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("File name is null or empty.");
        }

        if (separatorRegex == null || separatorRegex.isEmpty()) {
            throw new IllegalArgumentException("Separator regex is null or empty.");
        }

        String filePath = FileSystems.getDefault().getPath(DataConstants.RESOURCES_DIR, fileName).toAbsolutePath().toString();
        File file = new File(filePath);

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            return bufferedReader.lines()
                    .flatMap(line -> Stream.of(line.split(separatorRegex)))
                    .map(Alternative::createWith)
                    .collect(Collectors.toCollection(ArrayList<Alternative>::new));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            //TODO: Return something else than empty list
            return new ArrayList<>();
        }
    }

    /**
     * Load criteria weights from specified file
     * @param fileName
     * @param separatorRegex
     * @return
     */
    public static ArrayList<Criteria> loadCriteriaFrom(String fileName, String separatorRegex) {
        if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("File name is null or empty.");
        }

        if (separatorRegex == null || separatorRegex.isEmpty()) {
            throw new IllegalArgumentException("Separator regex is null or empty.");
        }

        String filePath = FileSystems.getDefault().getPath(DataConstants.RESOURCES_DIR, fileName).toAbsolutePath().toString();
        File file = new File(filePath);

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            return bufferedReader.lines()
                    .flatMap(line -> Stream.of(line.split(separatorRegex)))
                    .map(Criteria::createWith)
                    .collect(Collectors.toCollection(ArrayList<Criteria>::new));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            //TODO: Return something else than empty list
            return new ArrayList<>();
        }
    }

    /**
     * Loads performance table from specified file.
     * @param fileName
     * @param separatorRegex
     * @return
     */
    public static ArrayList<List<Integer>> loadPerformanceTableFrom(String fileName, String separatorRegex) {
        if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("File name is null or empty.");
        }

        if (separatorRegex == null || separatorRegex.isEmpty()) {
            throw new IllegalArgumentException("Separator regex is null or empty.");
        }

        String filePath = FileSystems.getDefault().getPath(DataConstants.RESOURCES_DIR, fileName).toAbsolutePath().toString();
        File file = new File(filePath);

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            return bufferedReader.lines()
                    .flatMap(line -> Stream.of(Arrays.asList(line.split(separatorRegex))))
                    .map(alternativeStringValues -> {
                        List<Integer> alternativeIntegerValues = new ArrayList<>();
                        for (String alternativeValue : alternativeStringValues) {
                            alternativeIntegerValues.add(Integer.valueOf(alternativeValue));
                        }
                        return alternativeIntegerValues;
                    })
                    .collect(Collectors.toCollection(ArrayList<List<Integer>>::new));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            //TODO: Return something else than empty list
            return new ArrayList<>();
        }
    }

    public static ArrayList<PrometheeConstraint> loadCriteriaConstraintsFrom(String fileName, String separatorRegex) {
        if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("File name is null or empty.");
        }

        if (separatorRegex == null || separatorRegex.isEmpty()) {
            throw new IllegalArgumentException("Separator regex is null or empty.");
        }

        String filePath = FileSystems.getDefault().getPath(DataConstants.RESOURCES_DIR, fileName).toAbsolutePath().toString();
        File file = new File(filePath);

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            return bufferedReader.lines()
                    .map(line -> {
                        String[] constraintLine = line.split(separatorRegex);
                        if (constraintLine.length != 3) {
                            throw new IllegalArgumentException();
                        }
                        //TODO: Cheack all the constraint parameters
                        return new PrometheeConstraint(
                                Criteria.createWith(constraintLine[0]),
                                promethee_v.model.Relationship.getRelationship(constraintLine[1]),
                                Double.valueOf(constraintLine[2]));
                    })
                    .collect(Collectors.toCollection(ArrayList<PrometheeConstraint>::new));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}
