package promethee_v.model;

/**
 * Created by biela.arek@gmail.com (Arek Biela) on 23.12.2016.
 */
public enum PreferenceTactics {

    V_SHAPE_WITH_INDIFFERENCE() {

        @Override
        public double getPreferenceValue(double criteriaValueFirstAlternative, double criteriaValueSecondAlternative, double criteriaPreferenceThreshold, double criteriaIndifferenceThreshold) {
            if (criteriaValueFirstAlternative - criteriaValueSecondAlternative > criteriaPreferenceThreshold)
                return 1;

            else if (criteriaValueFirstAlternative - criteriaValueSecondAlternative > criteriaIndifferenceThreshold)
                return (criteriaValueFirstAlternative - criteriaValueSecondAlternative - criteriaIndifferenceThreshold)/
                        (criteriaPreferenceThreshold - criteriaIndifferenceThreshold);

            else
                return 0;
        }
    };

    /**Retrieve preferenceValue */
    abstract public double getPreferenceValue(double criteriaValueFirstAlternative, double criteriaValueSecondAlternative,
                                              double criteriaPreferenceThreshold, double criteriaIndifferenceThreshold);
}
