package promethee_v.model;

import java.util.Objects;

/**
 * Model of typical MCDM problem criteria object.
 *
 * Created by biela.arek@gmail.com (Arek Biela) on 20.12.2016.
 */
public class Criteria {

    private final String name;

    private Criteria(String name) {
        this.name = name;
    }

    public static Criteria createWith(String name) {
        return new Criteria(name);
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Criteria criteria = (Criteria) o;
        return Objects.equals(name, criteria.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
