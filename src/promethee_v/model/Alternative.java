package promethee_v.model;

import java.util.Objects;

/**
 * Model of typical MCDM problem alternative object.
 *
 * Created by biela.arek@gmail.com (Arek Biela) on 20.12.2016.
 */
public class Alternative {

    private final String name;

    private Alternative(String name) {
        this.name = name;
    }

    public static Alternative createWith(String name) {
        return new Alternative(name);
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alternative that = (Alternative) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
