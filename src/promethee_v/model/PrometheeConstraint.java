package promethee_v.model;

/**
 * PrometheeV linear constraint model.
 *
 * Created by biela.arek@gmail.com (Arek Biela) on 20.12.2016.
 */
public class PrometheeConstraint {

    /**Criteria associated with the constraint*/
    private final Criteria criteria;
    /**Constraint's relationship*/
    private final Relationship relationship;
    /**Constraint's border value*/
    private final double constraintValue;

    public PrometheeConstraint(Criteria criteria, Relationship relationship, double constraintValue) {
        this.criteria = criteria;
        this.relationship = relationship;
        this.constraintValue = constraintValue;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public Relationship getRelationship() {
        return relationship;
    }

    public double getConstraintValue() {
        return constraintValue;
    }
}
