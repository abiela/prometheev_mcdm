package promethee_v.model;

/**
 * Created by biela.arek@gmail.com (Arek Biela) on 24.12.2016.
 */

public enum Relationship {
    EQUAL("="),
    GREATER_OR_EQUAL(">="),
    LESS_OR_EQUAL("<="),
    EMPTY("");

    private String stringValue;

    Relationship(String stringValue) {
        this.stringValue = stringValue;
    }

    public static Relationship getRelationship(String stringValue) {
        for (Relationship relationship : values()) {
            if (relationship.stringValue.equals(stringValue)) {
                return relationship;
            }
        }
        return EMPTY;
    }
}