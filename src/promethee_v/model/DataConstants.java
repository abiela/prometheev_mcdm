package promethee_v.model;

/**
 * Created by biela.arek@gmail.com (Arek Biela) on 20.12.2016.
 */
public class DataConstants {

    public static final String RESOURCES_DIR = "assets/";

    /**
     * Cars set
     */

    public static final String CARS_ALTERNATIVES = "carsAlternatives.csv";
    public static final String CARS_CRITERIA = "carsCriteria.csv";
    public static final String CARS_PERFORMANCE_TABLE = "carsPerformanceTable.csv";
    public static final String CARS_CONSTRAINTS = "carsConstraints.csv";

    /**
     * German credit set
     */

    public static final String GERMAN_CREDITS_ALTERNATIVES = "germanCreditAlternatives.csv";
    public static final String GERMAN_CREDITS_CRITERIA = "germanCreditCriteria.csv";
    public static final String GERMAN_CREDITS_PERFORMANCE_TABLE = "germanCreditPerformanceTable.csv";
    public static final String GERMAN_CREDITS_CONSTRAINTS = "germanCreditsConstraints.csv";
}
